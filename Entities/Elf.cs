﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.EQL.AI110.ChristmasFactory.Entities
{
    public class Elf
    {
        //Règles de gestions
        public const int NAME_MIN_LENGTH = 3;

        //PROPERTIES
        public int Id { get; set; }

        [Required(ErrorMessage = "champ nom obligatoire")]
        [MinLength(3, ErrorMessage ="Taille de champ incorrecte")]
        public string Name { get; set; }
        public bool IsAvailable { get; set; }
        public string Picture { get; set; }

        //CONSTRUCTORS
        public Elf()
        {
        }
        public Elf(int id, string name, bool isAvailable, string picture)
        {
            this.Id = id;
            this.Name = name;
            this.IsAvailable = isAvailable;
            this.Picture = picture;
        }

        //TO STRING
        public override string? ToString()
        {
            return this.Id + " - " + this.Name;
        }
    }

}
