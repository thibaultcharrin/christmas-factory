﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.EQL.AI110.ChristmasFactory.Entities
{
    public class Toy
    {
        //Règles de gestion
        public const int MIN_PRICE = 0;
        public const int MAX_PRICE = 10000;

        //les régions balisées par des # sont des 'directives' (pas compilées)
        #region ATTRIBUTES  
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        [Range(MIN_PRICE, MAX_PRICE, ErrorMessage = "Le prix n'est pas conforme")]
        public float Price { get; set; }
        public int? Id_Responsable { get; set; }    //version 'nullable' du type primitif accompagnée d'un '?'

        public Toy(int? id_Responsable)
        {
            Id_Responsable = id_Responsable;
        }
        #endregion

        #region CONSTRUCTORS
        public Toy()
        {
        }

        public Toy(int id, string name, string description, float price)
        {
            Id = id;    //this. n'est pas nécessaire
            Name = name;
            Description = description;
            Price = price;
        }
        #endregion
    }
}
