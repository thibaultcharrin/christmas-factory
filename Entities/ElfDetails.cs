﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.EQL.AI110.ChristmasFactory.Entities
{
    public class ElfDetails : Elf
    {
        public ElfDetails(Elf elf)
        {
            Id = elf.Id;
            Name = elf.Name;
            IsAvailable = elf.IsAvailable;
            Picture = elf.Picture;
        }
        public string CategoryName { get; set; }
    }
}
