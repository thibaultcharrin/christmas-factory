#!/bin/bash

#VARIABLES
VOLUME_NAME=christmas-mysql-volume

#STEPS
echo "*** Stopping Deployment ***"
docker compose down
echo "*** Removing residual elements ***"
echo "WARNING! Do you wish to remove the docker persitent volume $VOLUME_NAME? (all data will be lost) "
read -p "(y/n) ? " commit
if [ ${commit:=n} != "y" ]
        then
	echo "no volume was removed "
else
	docker volume rm $VOLUME_NAME
fi
echo "Do you wish to remove any present binary and clean the project? "
read -p "(y/n) ? " commit
if [ ${commit:=n} != "y" ]
        then
	echo "no file was removed "
else
	rm -Rf out/
	dotnet clean
fi
