CREATE DATABASE  IF NOT EXISTS `christmas_bdd` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `christmas_bdd`;
-- MySQL dump 10.13  Distrib 8.0.29, for Win64 (x86_64)
--
-- Host: localhost    Database: christmas_bdd
-- ------------------------------------------------------
-- Server version	8.0.29

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `elf`
--

DROP TABLE IF EXISTS `elf`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `elf` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `is_available` tinyint NOT NULL DEFAULT '1',
  `picture` varchar(255) DEFAULT NULL,
  `id_elf_cat` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_UNIQUE` (`name`),
  KEY `FK_elf_cat_idx` (`id_elf_cat`),
  CONSTRAINT `FK_elf_cat` FOREIGN KEY (`id_elf_cat`) REFERENCES `elf_category` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `elf`
--

LOCK TABLES `elf` WRITE;
/*!40000 ALTER TABLE `elf` DISABLE KEYS */;
INSERT INTO `elf` VALUES (1,'Link',1,'link.jpg',1),(3,'Legolas',1,'legolas.jpg',2),(4,'Zelda',1,'zelda.jpg',3),(6,'Benoit',0,'benoit.jpg',4),(12,'Aragorn',1,'aragorn.jpg',5),(16,'Jean-Pierre',1,'jp.jpg',6),(17,'Jean-Bernard',1,'jb.jpg',7),(18,'Thibault',1,'thibault.jpg',8),(19,'Sram',1,'sram.jpg',9),(20,'Iop',1,'iop.jpg',10),(21,'Pikachu',1,'pika.jpg',11);
/*!40000 ALTER TABLE `elf` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `elf_category`
--

DROP TABLE IF EXISTS `elf_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `elf_category` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `elf_category`
--

LOCK TABLES `elf_category` WRITE;
/*!40000 ALTER TABLE `elf_category` DISABLE KEYS */;
INSERT INTO `elf_category` VALUES (1,'Aquatic elf'),(2,'Dark elf'),(3,'Deep elf'),(4,'Grey elf'),(5,'High Elf'),(6,'Moon Elf'),(7,'Snow Elf'),(8,'Sun Elf'),(9,'Valley Elf'),(10,'Wild Elf'),(11,'Wood Elf'),(12,'Winged Elf'),(13,'Benoit');
/*!40000 ALTER TABLE `elf_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `toy`
--

DROP TABLE IF EXISTS `toy`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `toy` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` text,
  `price` float NOT NULL DEFAULT '0',
  `id_responsable` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_UNIQUE` (`name`),
  KEY `fk_toy_elf_idx` (`id_responsable`),
  CONSTRAINT `fk_toy_elf` FOREIGN KEY (`id_responsable`) REFERENCES `elf` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `toy`
--

LOCK TABLES `toy` WRITE;
/*!40000 ALTER TABLE `toy` DISABLE KEYS */;
INSERT INTO `toy` VALUES (1,'Xbox360','Microsoft',99.99,6),(2,'Switch','Nintendo',299.99,1),(3,'N64','Nintendo',12.99,3),(4,'GameCube','Nintendo',34.99,4),(5,'PS2','Sony',89.99,12),(6,'RogStrix','MSI',2899.99,16),(8,'Playstation 5','Sony',599.99,1),(9,'Nintendo 3DS','Special Edition',199.99,NULL),(10,'JUMANGI','Le Chaos',666,6),(11,'GameBoy Advance','Nintendo',19.99,NULL);
/*!40000 ALTER TABLE `toy` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'christmas_bdd'
--

--
-- Dumping routines for database 'christmas_bdd'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-07-12 12:02:47
