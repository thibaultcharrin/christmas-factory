﻿using Fr.EQL.AI110.ChristmasFactory.Business;
using Fr.EQL.AI110.ChristmasFactory.Entities;
using Fr.EQL.AI110.ChristmasFactory.Web.Models;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace Fr.EQL.AI110.ChristmasFactory.Web.Controllers
{
    public class ToyController : Controller
    {
        /*
        //Possible de REMPLACER l'URL
        [HttpGet]
        [Route("Nouveau-Jouet")]    //Annotation, Decorator
        */
        //Factorisation
        private IActionResult EditerJouet(string rubrique, Toy toy)
        {
            ElfBusiness elfBu = new ElfBusiness();
            List<Elf> elves = elfBu.GetElves();
            ViewBag.ListeResponsables = elves;
            ViewBag.Rubrique = rubrique;
            return View("Edit", toy);
        }

        #region CREATE

        // ***** GET/POST *****

        // attribute httpGet
        [HttpGet]   // action sur GET
        public IActionResult Create()
        {
            return EditerJouet("Création d'un Jouet", null);
        }
        [HttpPost]  //Java : Annotation (@override)     C# : Attribute
        public IActionResult Create(Toy toy)
        {
            if (ModelState.IsValid)
            {
                ToyBusiness toyBu = new ToyBusiness();
                toyBu.SaveToy(toy);
                return RedirectToAction("Index");   //redirection = pointer vers une action
            }
            else
            {
                //Si pas valide, j'affiche à nouveau la vue
                return EditerJouet("Création d'un Jouet", toy);
            }

        }

        //CORRIGE
        // attribute httpGet
        /*
        [HttpGet]   // action sur GET
        public IActionResult Corrige_Create()
        {
            return View();
        }
        [HttpPost]  //Java : Annotation (@override)     C# : Attribute
        public IActionResult Corrige_Create(Toy toy)
        {
            ToyBusiness tb = new ToyBusiness();
            tb.CreateToy(toy);
            return RedirectToAction("Index");   //redirection = pointer vers une action
        }
        */

        // ***** DEMO VIEWDATA *****

        //VIEWDATA
        public IActionResult Corrige_create()
        {
            ElfBusiness elfBu = new ElfBusiness();
            List<Elf> elves = elfBu.GetElves();

            ViewData.Add("listeResponsables", elves);
            ViewData.Add("titre", "plop");
            ViewData.Add("reponse", 42);
            return View();
        }
        #endregion

        #region READ
        [HttpGet]
        public IActionResult Index()
        {
            //Récupérer le Model
            ToySearchViewModel model = new ToySearchViewModel();

            try
            {
                ToyBusiness toyBu = new ToyBusiness();
                model.Toys = toyBu.SearchToys(model.Id_Responsable, model.ToyName, model.Min_Price, model.Max_Price);

                ElfBusiness ElfBu = new ElfBusiness();
                model.Elves = ElfBu.GetElves();

                // Charger la vue
                return View(model);
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = ex.Message;
                return View("Error");
            }
        }

        //RESULTAT RECHERCHE MULTICRITERE
        [HttpPost]
        public IActionResult Index(ToySearchViewModel model)
        {
            ToyBusiness toyBu = new ToyBusiness();
            model.Toys = toyBu.SearchToys(
                model.Id_Responsable, 
                model.ToyName, 
                model.Min_Price, 
                model.Max_Price);

            ElfBusiness ElfBu = new ElfBusiness();
            model.Elves = ElfBu.GetElves();

            return View(model);
        }

        #endregion

        #region UPDATE
        [HttpGet]
        public IActionResult Update(int id)
        {
            ToyBusiness tb = new ToyBusiness();
            Toy toy = tb.GetToyById(id);
            return EditerJouet("Modification d'un Jouet", toy);
        }

        [HttpPost]
        public IActionResult Update(Toy toy)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    ToyBusiness tb = new ToyBusiness();
                    tb.SaveToy(toy);
                    return RedirectToAction("Index");
                }
                catch (Exception ex)
                {
                    ViewBag.ErrorMessage = ex.Message;
                    return EditerJouet("Modification d'un Jouet", toy);
                }
            }
            else
            {
                return EditerJouet("Modification d'un Jouet", toy);
            }
        }
        #endregion

        #region DELETE
        public IActionResult Delete(int id)
        {
            ToyBusiness tb = new ToyBusiness();
            tb.DeleteToy(id);
            return RedirectToAction("Index");           
        }
        #endregion
    }
}
