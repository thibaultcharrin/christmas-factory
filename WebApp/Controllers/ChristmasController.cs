﻿using Microsoft.AspNetCore.Mvc;
using WebApp.Models;

namespace WebApp.Controllers
{
    public class ChristmasController : Controller
    {
        // Exemple utilisation du QueryString
        public IActionResult ExempleQS()
        {
            // /christmas/exempleqs?word=hello&nb=42

            // récupérer les paramètres :
            string word = Request.Query["word"];
            int nb = int.Parse(Request.Query["nb"]);

            string traduction = "";
            for (int i = 0; i < nb; i++)
            {
                traduction += word + "<br/>";
            }
            Response.ContentType = "text/html";
            return Content(traduction);

        }
        public IActionResult Welcome()
        {
            return View();
        }
        public IActionResult Voyage()
        {
            VoyageLointain v = new VoyageLointain();
            v.Destination = "Japon";
            //v.Destination = v.Destination.ToUpper();  //ATTENTION : Controleur viens chercher la donnée brut. Elle n'a pas vocation d'être transformée ici.
            v.Duree = 11;
            v.Activities.Add("shopping");
            v.Activities.Add("tatouages");
            v.Activities.Add("restaurant");
            return View(v);
        }

        public IActionResult Contact()
        {
            //si ma requête est en get
            if (Request.Method == "GET")
            {
                //je charge la vue par défaut:
                return View();
            }
            else
            {
                //je dois récupérer les données postées : Request.Form
                string email = Request.Form["email"];
                string message = Request.Form["message"];
                //je specifie explicitement le contenu
                Response.ContentType = "text/html;charset=utf-8";
                //sinon, c'est du 'plain text' donc pas interpreté
                return Content("<h1>Message bien reçu...</h1> message : " + 
                    message + "<br> mail : " + email);
            }
            
        }

    }
}
