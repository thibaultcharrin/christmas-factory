﻿using Fr.EQL.AI110.ChristmasFactory.Business;
using Fr.EQL.AI110.ChristmasFactory.Entities;
using Fr.EQL.AI110.ChristmasFactory.Web.Models;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace Fr.EQL.AI110.ChristmasFactory.Web.Controllers
{
    public class ElfController : Controller
    {
        //Factorisation
        private IActionResult EditerElfe(string rubrique, Elf elf)
        {
            ViewBag.Rubrique = rubrique;
            return View("Edit", elf);
        }

        #region CREATE

        #region ASP GET/POST
        // attribute httpGet
        [HttpGet]   // action sur GET
        public IActionResult Create()
        {
            return EditerElfe("Création d'un Elfe", null);
        }

        [HttpPost]  //Java : Annotation (@override)     C# : Attribute
        public IActionResult Create(Elf elf)
        {
            //J'enregistre mon Elfe uniquement s'il respecte les règles de validation
            if (ModelState.IsValid)
            {
                ElfBusiness bu = new ElfBusiness();
                bu.SaveElf(elf);
                return RedirectToAction("Index");   //redirection = pointer vers une action
            }
            else
            {
                //Si pas valide, j'affiche à nouveau la vue
                return EditerElfe("Création d'un Elfe", elf);
            }
              
        }
        #endregion

        #region GET/POST MANUELLE (DONT USE)
        public IActionResult Nouveau()
        {
            //si ma requête est en get
            if (Request.Method == "GET")
            {
                //je charge la vue par défaut:
                return View();
            }
            else
            {
                //je dois récupérer les données postées : Request.Form
                string nom = Request.Form["nom"];
                string image = Request.Form["image"];
                string dispo = Request.Form["dispo"];

                dispo = dispo != null ? "Oui" : "Non";
                bool check = dispo.Equals("Oui") ? true : false;

                //je specifie explicitement le contenu
                Response.ContentType = "text/html;charset=utf-8";
                //sinon, c'est du 'plain text' donc pas interpreté

                ElfBusiness eb = new ElfBusiness();
                Elf elf = new Elf();
                elf.Name = nom;
                elf.IsAvailable = check;
                elf.Picture = image;
                eb.SaveElf(elf);
                return Content("<h1>Vous avez créé un nouvel Elfe...</h1> Nom : " +
                    nom + "<br> Image : " + image + "<br> Dispo : " + dispo);
            }

        }
        #endregion
        #endregion

        #region READ
        [HttpGet]
        public IActionResult Index()
        {
            //Récupérer le Model
            ElfSearchViewModel model = new ElfSearchViewModel();

            try
            {
                ElfBusiness elfBu = new ElfBusiness();
                model.ElvesDetails = elfBu.SearchElf(0, "", "");

                // Charger la vue
                return View(model);
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = ex.Message;
                return View("Error");
            }
        }

        //RESULTAT RECHERCHE MULTICRITERE
        [HttpPost]
        public IActionResult Index(ElfSearchViewModel model)
        {
            ElfBusiness elfBu = new ElfBusiness();
            model.ElvesDetails = elfBu.SearchElf(
                model.ElfId,
                model.ElfName,
                model.ElfCategory);

            return View(model);
        }

        #region ROUTAGE
        //Methode 2 (GitLab Style)
        // affichage d'un elfe avec id dans la route :
        public IActionResult Infos(int id)
        {
            ElfBusiness eb = new ElfBusiness();
            return View("Detail", eb.GetElfById(id)); 
        }

        //Methode 1 (Google Style)
        public IActionResult Detail()
        {
            Elf elf = new Elf();
            //Mock (bouchon)

            int nb = int.Parse(Request.Query["id"]);

            ElfBusiness eb = new ElfBusiness();
            return View(eb.GetElfById(nb));
        }
        #endregion
        #endregion

        #region UPDATE
        [HttpGet]
        public IActionResult Update(int id)
        {
            ElfBusiness eb = new ElfBusiness();
            return EditerElfe("Modification d'un Elfe", eb.GetElfById(id));
        }

        [HttpPost]
        public IActionResult Update(Elf elf)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    ElfBusiness bu = new ElfBusiness();
                    bu.SaveElf(elf);
                    return RedirectToAction("Index");
                }
                catch (Exception ex)
                {
                    ViewBag.ErrorMessage = ex.Message;
                    return EditerElfe("Modification d'un Elfe", elf);
                }
            }
            else
            {
                return EditerElfe("Modification d'un Elfe", elf);
            }
        }
        #endregion
    }
}
