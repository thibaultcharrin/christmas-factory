﻿using Fr.EQL.AI110.ChristmasFactory.Entities;

namespace Fr.EQL.AI110.ChristmasFactory.Web.Models
{
    public class ToySearchViewModel
    {
        #region CRITERES DE RECHERCHE
        public int Id_Responsable { get; set; }
        public string ToyName { get; set; }
        public int Min_Price { get; set; }
        public int Max_Price { get; set; } = 100000;


        #endregion


        #region LISTES DE DONNEES
        public List<Elf> Elves { get; set; }
        public List<ToyDetails> Toys { get; set; }
        #endregion
    }
}
