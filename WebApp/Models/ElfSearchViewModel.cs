﻿using Fr.EQL.AI110.ChristmasFactory.Entities;

namespace Fr.EQL.AI110.ChristmasFactory.Web.Models
{
    public class ElfSearchViewModel
    {
        #region CRITERES DE RECHERCHE
        public int ElfId { get; set; }
        public string ElfName { get; set; }
        public string ElfCategory { get; set; }

        #endregion


        #region LISTES DE DONNEES
        public List<ElfDetails> ElvesDetails { get; set; }
        #endregion
    }
}
