﻿namespace WebApp.Models
{
    public class VoyageLointain
    {
        public string Destination { get; set; }
        public List<string> Activities { get; set; }
        public int Duree { get; set; }

        public VoyageLointain()
        {
            this.Activities = new List<string>();
        }


    }
}
