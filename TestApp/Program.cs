﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Fr.EQL.AI110.ChristmasFactory.Business;
//using Fr.EQL.AI110.ChristmasFactory.DataAccess;   //Desormais géré par Business
using Fr.EQL.AI110.ChristmasFactory.Entities;

namespace TestApp
{
    public class Program
    {
        public static void Main(string[] args)
        {
            TestInsertion();
            AfficherDetails();
            //TestMiseAJour();
            //TestSuppression();
            //AfficherTous();
        }

        #region METHODS

        //CREATE
        public static void TestInsertion()
        {
            Console.Write("Nom de l'elfe à ajouter ? ");
            string nom = Console.ReadLine();
            Elf e = new Elf(0, nom, true, "photo.jpg");
            Console.WriteLine(e.Name);
            ElfBusiness bu = new ElfBusiness();
            try
            {
                bu.SaveElf(e);
            }
            catch (Exception exc)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine(exc.Message);
                Console.ForegroundColor = ConsoleColor.Gray;
            }
            
        }

        //READ
        public static void AfficherDetails()
        {
            Console.WriteLine("ID de l'elfe ?");
            int id = int.Parse(Console.ReadLine());
            ElfBusiness bu = new ElfBusiness();     //Desormais géré par Business
            Elf elf = bu.GetElfById(id);
            try
            {
                if (elf != null)
                {
                    Console.WriteLine(
                        @"Nom : " +
                            elf.Name +
                        " ; ID : " +
                            elf.Id +
                        " ; Picture : " +
                            elf.Picture +
                        " ; Disponible : " +
                            ((elf.IsAvailable) ? "OUI" : "NON")
                        );
                }
                else Console.WriteLine("L'ID demandé n'existe pas.");
            }
            catch (Exception exc)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine(exc.Message);
                Console.ForegroundColor = ConsoleColor.Gray;
            }
        }

        public static void AfficherTous()
        {
            /*
            ElfDAO dao = new ElfDAO();
            List<Elf> elves = dao.GetAll();
            try
            {
                foreach (Elf elf in elves)
                {
                    Console.WriteLine(elf);
                }
            }
            catch (Exception exc)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine(exc.Message);
                Console.ForegroundColor = ConsoleColor.Gray;
            }
            */

        }

        //UPDATE
        public static void TestMiseAJour()
        {
            /*
            Console.WriteLine("ID à modifier ?");
            int id = int.Parse(Console.ReadLine());
            Console.WriteLine("Nouveau nom : ");
            string nom = Console.ReadLine();
            Elf e = new Elf(id, nom, true, "plop.jpg");
            ElfDAO dao = new ElfDAO();
            try
            {
                dao.Update(e);
            }
            catch (Exception exc)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine(exc.Message);
                Console.ForegroundColor = ConsoleColor.Gray;
            }
            */
        }

        //DELETE
        public static void TestSuppression()
        {
            /*
            Console.WriteLine("ID à supprimer ?");
            int id = int.Parse(Console.ReadLine());
            ElfDAO dao = new ElfDAO();
            try
            {
                dao.Delete(id);
            }
            catch (Exception exc)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine(exc.Message);
                Console.ForegroundColor = ConsoleColor.Gray;
            }    
            */
        }

        #endregion
    }
}
