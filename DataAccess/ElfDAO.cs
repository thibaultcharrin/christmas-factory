﻿using Fr.EQL.AI110.ChristmasFactory.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//NuGet dependency
using MySql.Data.MySqlClient;
using System.Data.Common;

namespace Fr.EQL.AI110.ChristmasFactory.DataAccess
{
    // Java : heritage 'extends'
    // C#   : heritage ':'
    public class ElfDAO : DAO
    {
        //Factorisation
        private Elf DataReaderToEntity(DbDataReader dr)
        {
            Elf elf = new Elf();
            elf.Id = dr.GetInt32(dr.GetOrdinal("id"));  //C# : Indexer <=> Java : dr.getValue("id")
            elf.Name = dr.GetString(dr.GetOrdinal("name"));
            elf.IsAvailable = dr.GetBoolean(dr.GetOrdinal("is_available"));
            if (!dr.IsDBNull(dr.GetOrdinal("picture")))
            {
                elf.Picture = dr.GetString(dr.GetOrdinal("picture"));
            }
            return elf;
        }

        #region CREATE
        public void Insert(Elf elf)
        {
            // Créer un objet connection
            // 1 - Paramétrer la connection :
            DbConnection cnx = new MySqlConnection(CNX_STR);

            // 2 - Paramétrer la commande :
            DbCommand cmd = cnx.CreateCommand();    // ici, '@' correspond au C# pour étendre la string
            cmd.CommandText = @"INSERT 
                                INTO elf
                                    (name, is_available, picture) 
                                VALUES 
                                    (@name, @isAvailable, @picture)";   // ici, '@' correspond au MySQL qui attend 3 paramètres

            cmd.Parameters.Add(new MySqlParameter("name", elf.Name));
            cmd.Parameters.Add(new MySqlParameter("isAvailable", elf.IsAvailable));
            cmd.Parameters.Add(new MySqlParameter("picture", elf.Picture));

            try
            {   
                // 3 - ouvrir la connection :
                cnx.Open();
                // 4 - Ecécuter la commande :
                // 5 - Traiter le résultat  :
                int nbLignes = cmd.ExecuteNonQuery();
                Console.WriteLine(nbLignes);
            }
            catch(MySqlException exc)
            {
                throw new Exception("Erreur lors de l'insertion d'un elfe : " + exc.Message);
            }
            finally
            {
                // 6 - fermer la connection :
                cnx.Close();
            }
        }
        #endregion

        #region READ
        public Elf GetById(int id)
        {
            Elf result = null;
            DbConnection cnx = new MySqlConnection(CNX_STR);
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = @"SELECT * 
                                FROM elf
                                WHERE id=@id";
            cmd.Parameters.Add(new MySqlParameter("id", id));
            try
            {
                cnx.Open();
                DbDataReader dr = cmd.ExecuteReader();
                if (dr.Read())
                {
                    result = new Elf();
                    result.Id = dr.GetInt32(dr.GetOrdinal("id"));
                    result.Name = dr.GetString(dr.GetOrdinal("name"));
                    result.IsAvailable = dr.GetBoolean(dr.GetOrdinal("is_available"));
                    if (!dr.IsDBNull(dr.GetOrdinal("picture")))
                    {
                        result.Picture = dr.GetString(dr.GetOrdinal("picture"));
                    }
                }
                return result;
            }
            catch (MySqlException exc)
            {
                throw new Exception("Erreur lors de la lecture d'un elfe : " + exc.Message);
            }
            finally { cnx.Close(); }
        }

        public List<ElfDetails> GetByMultiCriteria(
                int id,
                string name,
                string category)
        {
            List<ElfDetails> result = new List<ElfDetails>();
            DbConnection cnx = new MySqlConnection(CNX_STR);
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = @"SELECT e.*, c.name 'category' 
                                FROM elf e
                                LEFT JOIN elf_category c ON e.id_elf_cat = c.id
                                WHERE (e.id=@id OR @id = 0)
                                AND e.name LIKE @name
                                AND c.name LIKE @category";

            name = "%" + name + "%";
            category = "%" + category + "%";
            cmd.Parameters.Add(new MySqlParameter("name", name));
            cmd.Parameters.Add(new MySqlParameter("id", id));
            cmd.Parameters.Add(new MySqlParameter("category", category));
            try
            {
                cnx.Open();

                DbDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    Elf elf = DataReaderToEntity(dr);
                    ElfDetails elfDetails = new ElfDetails(elf);

                    if (!dr.IsDBNull(dr.GetOrdinal("category")))
                    {
                        elfDetails.CategoryName = dr.GetString(dr.GetOrdinal("category"));
                    }
                    result.Add(elfDetails);
                }
            }
            catch (MySqlException exc)
            {
                throw new Exception("Erreur lors de la lecture des jouets : " + exc.Message);
            }
            finally
            {
                // on ferme la connexion dans un finally pour
                // garantir qu'elle sera bien fermée quoiqu'il arrive
                cnx.Close();
            }
            return result;
        }
        public List<Elf> GetAll()
        {
            //            java:         C#:
            // interface: List          IList
            // classe   : ArrayList     List
            List<Elf> result = new List<Elf>();
            DbConnection cnx = new MySqlConnection(CNX_STR);
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = "SELECT * FROM elf";
            try
            {
                cnx.Open();
                DbDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Elf elf = new Elf();
                    elf.Id = dr.GetInt32(dr.GetOrdinal("id"));  //C# : Indexer <=> Java : dr.getValue("id")
                    elf.Name = dr.GetString(dr.GetOrdinal("name"));
                    elf.IsAvailable = dr.GetBoolean(dr.GetOrdinal("is_available"));
                    if (!dr.IsDBNull(dr.GetOrdinal("picture")))
                    {
                        elf.Picture = dr.GetString(dr.GetOrdinal("picture"));
                    }
                    result.Add(elf);
                }
                return result;
            }
            catch (MySqlException exc)
            {
                throw new Exception("Erreur lors de la lecture des elfes : " + exc.Message);
            }
            finally { cnx.Close(); }
        }
        #endregion

        #region UPDATE
        public void Update(Elf elf)
        {
            DbConnection cnx = new MySqlConnection(CNX_STR);
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = @"UPDATE elf
                                SET name = @name, 
                                    is_available = @isAvailable, 
                                    picture = @picture
                                WHERE 
                                    id = @id";
            cmd.Parameters.Add(new MySqlParameter("id", elf.Id));
            cmd.Parameters.Add(new MySqlParameter("name", elf.Name));
            cmd.Parameters.Add(new MySqlParameter("isAvailable", elf.IsAvailable));
            cmd.Parameters.Add(new MySqlParameter("picture", elf.Picture));
            try
            {
                cnx.Open();
                int nbLignes = cmd.ExecuteNonQuery();
                Console.WriteLine(nbLignes);
            }
            catch (MySqlException exc)
            {
                throw new Exception("Erreur lors de la MAJ d'un elfe : " + exc.Message);
            }
            finally { cnx.Close(); }
        }
        #endregion

        #region DELETE
        public void Delete(int id)
        {
            DbConnection cnx = new MySqlConnection(CNX_STR);
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = @"DELETE 
                                FROM elf
                                WHERE 
                                    id = @id";
            cmd.Parameters.Add(new MySqlParameter("id", id));
            try
            {
                cnx.Open();
                int nbLignes = cmd.ExecuteNonQuery();
                Console.WriteLine(nbLignes);
            }
            catch (MySqlException exc)
            {
                throw new Exception("Erreur lors de la suppression d'un elfe : " + exc.Message);
            }  
            finally
            {
                cnx.Close();
            }
        }
        #endregion
    }
}
