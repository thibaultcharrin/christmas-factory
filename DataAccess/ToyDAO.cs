﻿using Fr.EQL.AI110.ChristmasFactory.Entities;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.EQL.AI110.ChristmasFactory.DataAccess
{
    public class ToyDAO : DAO
    {
        //Factorisation
        private Toy DataReaderToEntity(DbDataReader dr)
        {
            Toy toy = new Toy();
            toy.Id = dr.GetInt32(dr.GetOrdinal("id"));  //C# : Indexer <=> Java : dr.getValue("id")
            toy.Name = dr.GetString(dr.GetOrdinal("name"));
            toy.Description = dr.GetString(dr.GetOrdinal("description"));
            toy.Price = dr.GetFloat(dr.GetOrdinal("price"));

            if (!dr.IsDBNull(dr.GetOrdinal("id_responsable")))
            {
                toy.Id_Responsable = dr.GetInt32(dr.GetOrdinal("id_responsable"));
            }
            return toy;
        }

        #region CREATE
        public void Insert(Toy toy)
        {
            // Créer un objet connection
            // 1 - Paramétrer la connection :
            DbConnection cnx = new MySqlConnection(CNX_STR);

            // 2 - Paramétrer la commande :
            DbCommand cmd = cnx.CreateCommand();    // ici, '@' correspond au C# pour étendre la string
            cmd.CommandText = @"INSERT 
                                INTO toy
                                    (name, description, price, id_responsable) 
                                VALUES 
                                    (@name, @description, @price, @id_responsable)";   // ici, '@' correspond au MySQL qui attend 3 paramètres

            cmd.Parameters.Add(new MySqlParameter("name", toy.Name));
            cmd.Parameters.Add(new MySqlParameter("description", toy.Description));
            cmd.Parameters.Add(new MySqlParameter("id_responsable", toy.Id_Responsable));
            cmd.Parameters.Add(new MySqlParameter("price", toy.Price));

            try
            {
                // 3 - ouvrir la connection :
                cnx.Open();
                // 4 - Ecécuter la commande :
                // 5 - Traiter le résultat  :
                int nbLignes = cmd.ExecuteNonQuery();
                Console.WriteLine(nbLignes);
            }
            catch (MySqlException exc)
            {
                throw new Exception("Erreur lors de l'insertion d'un jouet : " + exc.Message);
            }
            finally
            {
                // 6 - fermer la connection :
                cnx.Close();
            }
        }
        #endregion

        #region READ
        public Toy GetById(int id)
        {
            Toy result = null;
            DbConnection cnx = new MySqlConnection(CNX_STR);
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = @"SELECT * 
                                FROM toy
                                WHERE id=@id";
            cmd.Parameters.Add(new MySqlParameter("id", id));
            try
            {
                cnx.Open();

                DbDataReader dr = cmd.ExecuteReader();

                if (dr.Read())
                {
                    result = DataReaderToEntity(dr);
                }
            }
            catch (MySqlException exc)
            {
                throw new Exception("Erreur lors de la lecture d'un jouet : " + exc.Message);
            }
            finally
            {
                cnx.Close();
            }

            return result;
        }
        public List<ToyDetails> GetByMultiCriteria(
                int id_responsable, 
                string name, 
                int min_price, 
                int max_price)
        {
            List<ToyDetails> result = new List<ToyDetails>();
            DbConnection cnx = new MySqlConnection(CNX_STR);
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = @"SELECT t.*, e.name 'responsable' 
                                FROM toy t
                                LEFT JOIN elf e ON t.id_responsable = e.id
                                WHERE (id_responsable=@id_responsable OR @id_responsable = 0)
                                AND t.name LIKE @name
                                AND price >= @min_price
                                AND price <= @max_price";

            name = "%" + name + "%";
            cmd.Parameters.Add(new MySqlParameter("name", name));
            cmd.Parameters.Add(new MySqlParameter("id_responsable", id_responsable));
            cmd.Parameters.Add(new MySqlParameter("min_price", min_price));
            cmd.Parameters.Add(new MySqlParameter("max_price", max_price));
            try
            {
                cnx.Open();

                DbDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    Toy toy = DataReaderToEntity(dr);
                    ToyDetails toyDetails = new ToyDetails(toy);
                    
                    if(!dr.IsDBNull(dr.GetOrdinal("responsable")))
                    {
                        toyDetails.RespName = dr.GetString(dr.GetOrdinal("responsable"));
                    }

                    result.Add(toyDetails);
                }
            }
            catch (MySqlException exc)
            {
                throw new Exception("Erreur lors de la lecture des jouets : " + exc.Message);
            }
            finally
            {
                // on ferme la connexion dans un finally pour
                // garantir qu'elle sera bien fermée quoiqu'il arrive
                cnx.Close();
            }
            return result;
        }

        public List<Toy> GetAll()
        {
            List<Toy> result = new List<Toy>();
            DbConnection cnx = new MySqlConnection(CNX_STR);
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = "SELECT * FROM toy";
            try
            {
                cnx.Open();

                DbDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    Toy toy = DataReaderToEntity(dr);

                    result.Add(toy);
                }
            }
            catch (MySqlException exc)
            {
                throw new Exception("Erreur lors de la lecture des jouets : " + exc.Message);
            }
            finally
            {
                // on ferme la connexion dans un finally pour
                // garantir qu'elle sera bien fermée quoiqu'il arrive
                cnx.Close();
            }
            return result;
        }
        #endregion

        #region UPDATE
        public void Update(Toy toy)
        {
            DbConnection cnx = new MySqlConnection(CNX_STR);
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = @"UPDATE toy
                                SET name = @name, 
                                    description = @description, 
                                    price = @price,
                                    id_responsable = @id_responsable
                                WHERE 
                                    id = @id";
            cmd.Parameters.Add(new MySqlParameter("id", toy.Id));
            cmd.Parameters.Add(new MySqlParameter("name", toy.Name));
            cmd.Parameters.Add(new MySqlParameter("description", toy.Description));
            cmd.Parameters.Add(new MySqlParameter("price", toy.Price));
            cmd.Parameters.Add(new MySqlParameter("id_responsable", toy.Id_Responsable));
            try
            {
                cnx.Open();
                int nbLignes = cmd.ExecuteNonQuery();
                Console.WriteLine(nbLignes);
            }
            catch (MySqlException exc)
            {
                throw new Exception("Erreur lors de la MAJ d'un jouet : " + exc.Message);
            }
            finally { cnx.Close(); }
        }
        #endregion

        #region DELETE
        public void Delete(int id)
        {
            DbConnection cnx = new MySqlConnection(CNX_STR);
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = @"DELETE 
                                FROM toy
                                WHERE 
                                    id = @id";
            cmd.Parameters.Add(new MySqlParameter("id", id));
            try
            {
                cnx.Open();
                int nbLignes = cmd.ExecuteNonQuery();
                Console.WriteLine(nbLignes);
            }
            catch (MySqlException exc)
            {
                throw new Exception("Erreur lors de la suppression d'un jouet : " + exc.Message);
            }
            finally
            {
                cnx.Close();
            }
        }
        #endregion
    }
}
