#!/bin/bash

#VARIABLES
VOLUME_NAME=mysql-pv-volume

#STEPS
echo "*** Deleting namespace ***"
kubectl delete namespace production

echo "*** Removing residual elements ***"
echo "WARNING! Do you wish to remove the persitent volume $VOLUME_NAME? (all data will be lost) "
read -p "(y/n) ? " commit
if [ ${commit:=n} != "y" ]
        then
	echo "no volume was removed "
else
	kubectl delete pv $VOLUME_NAME
fi

echo "Do you wish to stop your Minikube Cluster? "
read -p "(y/n) ? " commit
if [ ${commit:=n} != "y" ]
        then
	echo "no cluster was stopped "
else
	minikube stop
fi

