﻿using Fr.EQL.AI110.ChristmasFactory.DataAccess;
using Fr.EQL.AI110.ChristmasFactory.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.EQL.AI110.ChristmasFactory.Business
{   
    //Granularité 'Use Cases'
    public class ElfBusiness
    {
        #region SAVE (CREATE+UPDATE)
        public void SaveElf(Elf elf)
        {
            ElfDAO dao = new ElfDAO();
            //appliquer les règles de gestion
            if (elf.Name.Length < Elf.NAME_MIN_LENGTH)
            {
                throw new Exception("Création impossible : le nom doit faire au moins " 
                    + Elf.NAME_MIN_LENGTH + " caractères");
            }
            if (elf.Id == 0)
            {
                dao.Insert(elf);
            }
            else
            {
                dao.Update(elf);
            }
                
        }
        #endregion

        #region CREATE
        /*
        public void CreateElf(Elf elf)
        {
            // appliquer les RDG (règles de gestions)
            if (elf.Name.Length < 3)
            {
                throw new Exception("Création impossible : le nom doit faire au moins 3 caractères");
            }
            // envoyer au DAO (Data Access Object)
            ElfDAO dao = new ElfDAO();
            dao.Insert(elf);
        }
        */
        #endregion

        #region READ
        public Elf GetElfById(int id)
        {
            /*
            ElfDAO dao = new ElfDAO();
            return dao.GetById(id); 
            */
            return new ElfDAO().GetById(id);    
        }
        public List<ElfDetails> SearchElf(
           int id,
           string name,
           string category)
        {
            ElfDAO dao = new ElfDAO();
            return dao.GetByMultiCriteria(id, name, category);
        }
        public List<Elf> GetElves()
        {
            ElfDAO dao = new ElfDAO();
            return dao.GetAll();
        }
        #endregion

        #region UPDATE
        /*
        public void UpdateElf(Elf elf)
        {
            // appliquer les RDG (règles de gestions)
            if (elf.Name.Length < 3)
            {
                throw new Exception("Création impossible : le nom doit faire au moins 3 caractères");
            }
            // envoyer au DAO (Data Access Object)
            ElfDAO dao = new ElfDAO();
            dao.Update(elf);
        }
        */
        #endregion
    }
}
