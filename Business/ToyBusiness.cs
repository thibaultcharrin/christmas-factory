﻿using Fr.EQL.AI110.ChristmasFactory.DataAccess;
using Fr.EQL.AI110.ChristmasFactory.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.EQL.AI110.ChristmasFactory.Business
{
    public class ToyBusiness
    {
        #region SAVE (CREATE+UPDATE)
        public void SaveToy(Toy toy)
        {
            ToyDAO dao = new ToyDAO();
            //appliquer les règles de gestion
            if (toy.Price < Toy.MIN_PRICE || toy.Price > Toy.MAX_PRICE)
            {
                throw new Exception("La valeur entrée n'est pas valide");
            }
            if (toy.Id == 0)
            {
                dao.Insert(toy);
            }
            else
            {
                dao.Update(toy);
            }
        }
        #endregion

        #region READ
        public Toy GetToyById(int id)
        {
            ToyDAO dao = new ToyDAO();
            return dao.GetById(id);
        }

        public List<ToyDetails> SearchToys(
            int id_responsable, 
            string name, 
            int min_price, 
            int max_price)
        {
            ToyDAO dao = new ToyDAO();
            return dao.GetByMultiCriteria(id_responsable, name, min_price, max_price);
        }

        public List<Toy> GetToys()
        {
            ToyDAO dao = new ToyDAO();
            return dao.GetAll();
        }
        #endregion

        #region CREATE
        public void CreateToy(Toy toy)
        {
            // appliquer les RDG (règles de gestions)
            if (toy.Price < Toy.MIN_PRICE || toy.Price > Toy.MAX_PRICE)
            {
                throw new Exception("Création impossible : le prix doit-être entre 0 et 10000");
            }
            // envoyer au DAO (Data Access Object)
            ToyDAO dao = new ToyDAO();
            dao.Insert(toy);
        }
        #endregion

        #region DELETE
        public void DeleteToy(int id)
        {
            ToyDAO dao = new ToyDAO();
            dao.Delete(id);
        }
        #endregion
    }
}